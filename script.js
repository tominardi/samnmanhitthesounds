document.addEventListener('DOMContentLoaded', function() {
    var SOUNDS = [
        {'title': 'Hurler comme un forcené', 'src': 'sounds/hurlercommeunforcene.mp3'},
        {'title': 'Lincoln Tunnel', 'src': 'sounds/lincolntunnel.mp3'},
        {'title': 'Pas un vrai mec', 'src': 'sounds/pasunvraimec.mp3'},
        {'title': 'Pronto', 'src': 'sounds/pronto.mp3'},
        {'title': 'Adorable Galopin', 'src': 'sounds/adorablegalopin.mp3'},
        {'title': 'Brutal', 'src': 'sounds/brutal.mp3'},
        {'title': 'C\'est à moi que vous parlez ?', 'src': 'sounds/cestamoiquevousparlez.mp3'},
        {'title': 'De quoi diable parles-tu ?', 'src': 'sounds/dequoidiable.mp3'},
        {'title': 'Hypercephale', 'src': 'sounds/hypercephale.mp3'},
        {'title': 'Monsieur Bosco', 'src': 'sounds/monsieurbosco.mp3'},
        {'title': 'Que des étrangers là dehors', 'src': 'sounds/quedesetrangers.mp3'},
        {'title': 'Smicard', 'src': 'sounds/smicard.mp3'},
        {'title': 'Violence Gratuite', 'src': 'sounds/violencegratuite.mp3'},
        {'title': 'Idées turgides', 'src': 'sounds/ideesturgides.mp3'},
        {'title': 'C\'est cool', 'src': 'sounds/cestcool.mp3'},

    ];

    var soundsWrapper = document.getElementById("soundsWrapper");
    SOUNDS.forEach((function(sound) {
        var button = document.createElement('button');
        button.textContent = sound.title;
        button.setAttribute('data-src', sound.src);
        button.className = "sound";
        var buttonWrapper = document.createElement('div');
        buttonWrapper.className = 'buttonWrapper';
        buttonWrapper.appendChild(button);
        soundsWrapper.appendChild(buttonWrapper);
    }).bind(this));

    var soundPlayer = document.createElement('audio');
    soundPlayer.setAttribute('id', 'player');
    document.body.appendChild(soundPlayer);

    var sounds = document.getElementsByClassName('sound');

    for (var i = 0; i < sounds.length; i++) {
        sounds[i].addEventListener('click', function() {
            var url = this.getAttribute('data-src');
            var player = document.getElementById('player');
            player.src = url;
            player.play();
        });
    }


});
